#!/usr/bin/env pwsh
# Advent of Code 2017 (Powershell)
# {{{title}}} (Part {{{part}}})

Import-Module ./common.psm1

function Solve($input)
{
    return $input
}

If(IsMain) {
  If(IsTest) {
    Test "Example 1" $(Solve "13") -Equals "13"
    Test "Example 2" $(Solve Get-Input("template.example")) -Equals "1337"
    Test "Solve" $(Solve("123"))
  }
  else
  {
    Solve(Get-Input("{{{day}}}"))
  }
}