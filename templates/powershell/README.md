# Advent of Code Starter Kit - Powershell

## Quickstart

Assuming all statements are run from the repository root.

```
# Initialise a new Powershell solution for Day 01 challenge
utils/aoc-init 1 ps

# Run tests
pwsh day01a.ps1 test

# Solve and print answer
pwsh day01a.ps1 
```

## Why use Powershell?

https://github.com/PowerShell/PowerShell

Don't. Really. Just don't.

## Installing pre-requisites

Solutions assume Powershell Core for Linux. They *should* work on standard Powershell on
Windows but are untested. 