# Advent of Code polyglot starter kit

function Get-Input($filename) {
  $path = [System.IO.Path]::GetFullPath("$PSScriptRoot/../../challenges/$filename.input")
  return [string]$(Get-Content "$path")
}

function IsTest() {
  $args = (Get-Variable MyInvocation -Scope Global).Value.UnboundArguments
  if($args.count -Eq 0)
  {
    return $false
  }

  return ($args[0] -eq 'test')
}

function IsMain() {
  (Get-Variable MyInvocation -Scope Local).Value.PSCommandPath -Eq (Get-Variable MyInvocation -Scope Global).Value.InvocationName
}

# TODO: would be cool if this can take deferred statement instead of value for
#       expected value and capture exceptions as failures instead of just throwing
function Test
{
  Param (
    [Parameter(Mandatory=$true, Position=0)] [string] $Description,
    [Parameter(Mandatory=$true, Position=1)] $Assert,
    [Parameter(Mandatory=$false, Position=2)] $Equals
  )

  if($Equals -Eq $null) # No expected value passed; just ensuring it runs
  {
    Write-Host "[????]  $Description"
    Write-Host "         Output:   " -NoNewline
    Write-Host "$Assert" -foregroundcolor "yellow"
  }
  Else
  {
    $PASS=$("$Assert" -Eq "$Equals")

    Write-Host "[" -NoNewline
    If($PASS)
      { Write-Host "PASS" -foregroundcolor "green" -NoNewline }
    Else
      { Write-Host "FAIL" -foregroundcolor "red" -NoNewline }
    Write-Host "] $Description ..."

    Write-Host "         Expected: $Equals"
    if($PASS){
      Write-Host "       ✓ " -foregroundcolor "green" -NoNewline
      Write-Host "Actual:   " -NoNewline
      Write-Host "$Assert " -foregroundcolor "darkgreen"
    }
    Else
    {
      Write-Host "       ✗ " -foregroundcolor "red" -NoNewline
      Write-Host "Actual:   " -NoNewline
      Write-Host "$Assert " -foregroundcolor "darkred"
    }
  }
  Write-Host "-------------------------------------------" -foregroundcolor "darkgray"
}

Export-ModuleMember -Function 'Get-Input'
Export-ModuleMember -Function 'Test'
Export-ModuleMember -Function 'IsMain'
Export-ModuleMember -Function 'IsTest'
