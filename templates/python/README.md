# Advent of Code Starter Kit - Python

## Quickstart

Assuming all statements are run from the repository root.

```
# Initialise a new Python solution for Day 01 challenge
utils/aoc-init 1 python

# Run tests
py.test solutions/python/day01a.py

# Solve and print answer
python solutions/python/day01a.py
```

## Why use Python?

https://www.python.org

Some useful resources for learning Python:

* http://mark-dot-net.blogspot.se/2014/03/python-equivalents-of-linq-methods.html

## Installing pre-requisites

Solutions assume Python 3.6.

Install Python 3.6 or higher: (link to go here)

Install pip: (todo)

Run `pip install pytest` to enable unit test support

NOTE: this all assumes you are using an operating system or distro with a sane approach to Python, i.e. Python 3 is the default and Python 2 can go live in the past where it belongs. In some environments where Python 2 is the default you may need to use different commands such as replacing `python` with `python3`, `pip` with `pip3` etc.