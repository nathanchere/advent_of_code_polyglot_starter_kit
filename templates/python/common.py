from os import path

def get_input(day):
    filePath = path.realpath(
        path.dirname(__file__) + f'/../../challenges/{day}.input'
        )
    with open(filePath,'r') as f:
        return f.read()