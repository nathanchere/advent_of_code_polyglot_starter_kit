# Advent of Code 2017 (Python)
# {{{title}}} (Part {{{part}}})

from common import *

def solve(input):
    return input

class Tests():
    import pytest
    #def test_example(x): assert solve(13) == 13
    #def test_example(x): assert solve(get_input('day{{{day}}}.example')) == '13'
    def test_solve(x): assert solve(get_input('day{{{day}}}'))

if __name__ == "__main__":
    print(solve(get_input('day{{{day}}}')))
