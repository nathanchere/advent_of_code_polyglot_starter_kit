# Advent of Code Starter Kit - LANG

## Quickstart

Assuming all statements are run from the repository root.

```
# Initialise a new LANG solution for Day 01 challenge
utils/aoc-init 1 LANG

# Run tests
dotnet test solutions/csharp

# Solve and print answer
dotnet run solutions/csharp/day01a.cs
```

## Why use LANG?

https://docs.microsoft.com/en-us/dotnet/csharp/

Some useful resources for learning LANG:

* LINKS

## Installing pre-requisites

Solutions assume .Net Core 2.0. TODO - standard version? works with full framework x?

Install LANG: (link to go here)
