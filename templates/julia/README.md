# Advent of Code Starter Kit - Julia

## Quickstart

Assuming all statements are run from the repository root.

```
# Initialise a new Python solution for Day 01 challenge
utils/aoc-init 1 python

# Run tests
julia solutions/julia/day01a.jl test

# Solve and print answer
julia solutions/julia/day01a.jl
```

## Why use Julia?

https://julialang.org/

Some useful resources for learning Julia:

* TODO

## Installing pre-requisites

Solutions assume Julia 0.6.

Install Julia 0.6 or higher: (link to go here)