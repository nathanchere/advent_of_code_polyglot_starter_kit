// Advent of Code 2017 (F#)
// {{{title}}} (Part {{{part}}})

#r "Expecto.dll"
#load "common.fsx"
open Common
open Expecto

let solve input =
  System.Int32.Parse(input)

[<Tests>]
let tests =
  testList "Tests" [
    test "Example1" {
      Expect.equal (solve 1234) '1234'
    }
    test "Example2" {
       Expect.equal (solve(getInput "day{{{day}}}.example")) 13
    }
    test "Solve" {
      solve(getInput(day"{{{day}}}"))
    }
  ]

if isTest then
  doTests tests
else
  solve(getInput(day"{{{day}}}"))
