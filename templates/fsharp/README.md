# Advent of Code Starter Kit - F#

## Quickstart

Assuming all statements are run from the repository root.

```
# Initialise a new Python solution for Day 01 challenge
utils/aoc-init 1 fsharp

# Run tests
fsharpi day01a.fs test

# Solve and print answer
fsharpi day01a.fs
```

## Why use F#?

http://fsharp.org

Some useful resources for learning F#:

* https://fsharpforfunandprofit.com
* https://en.wikibooks.org/wiki/F_Sharp_Programming
* https://dungpa.github.io/fsharp-cheatsheet/
* https://github.com/fsprojects/awesome-fsharp
* http://brandewinder.com/2016/02/06/10-fsharp-scripting-tips/

## Installing pre-requisites

Solutions assume F# 4.1
