# Advent of Code 2017 polyglot starter kit

## Why?

- Trying to make it easier for others to get started in new languages for Advent of Code with minimal yak shaving

- Providing helper scripts for the more competitive types who care about every wasted minute pushing them further down the global leaderboard

## Quickstart

1. clone locally
2. copy `utils/config.py.example` to `utils/config.py` and fill in the empty values
3. run `utils/aoc-init 1 python` to create an empty Python solution for day 1
4. write code, run, get answer
5. run `utils/aoc-submit 1 1 12345` to submit your answer of `12345` (or whatever it really is) for day 1 part 1

## Repository structure

`./challenges` contains challenge descriptions, example input and challenge input.

`./solutions` where empty solutions for each challenge are generated.

`./templates` contain minimal copies of the solution code for each language with additional comments where relevant.

`./utils` has a few Python scripts which I use for things like fetching challenge input and submitting my answers.

## Utils

Included are some helper scripts which are intended to take as much friction as possible out of the
process of solving challenges.

### Prerequisites

All helper scripts assume Python 3.6 - they may work with other 3.x versions but nothing is tested. Feel
free to open an issue if you find anything which can be easily fixed to make it work with more. This also
assumes you are using an operating system or distro with a sane approach to Python, i.e. Python 3 is the
default and Python 2 can go live in the past where it belongs.

Use `pip` to install library dependencies:

```
pip install beautifulsoup4 tomd
```

You will also need to create a `config.py` file in the `./utils` directory. There is an example provided
as `config.py.example` which you can copy to get started. The main value you need to fill in is `SESSION_ID`
which contains the cookie used to authenticate you against your Advent of Code account.

[Click here to see how to get your SESSION_ID value (GitHub)](https://github.com/wimglenn/advent-of-code/issues/1)

### fetch

**Usage**:

```
aoc-fetch [days]
```

Gets the challenge description and input files for any given day and saves them to `./challenges`. Challenge
instructions are saved as `dayXX.md` in Markdown format - view them as an ordinary text file or with the
markdown viewer of your choice (Haroopad is pretty neat!).

Note that the first time you fetch the description it will only have the first part of the challenge. Once you've sucessfully solved Part 1 of a given challenge you will need to fetch it again to get the description for Part 2 (unless you use `solve` - more on that later).

Example:

`aoc-fetch 4` - gets challenge description and input for day 4
`aoc-fetch 7 8 9` - gets challenge description and input for days 7, 8 and 9

### init

**Usage**:

```
aoc-init [day] [languages]
aoc-init list
```

Example:

`aoc-init`

### solve

**Usage**:

```
aoc-solve [day] [part] [answer]
```

Example:s

`aoc-init`

## Languages

I don't claim to be fluent in any of these langauges except perhaps C#. This is also a learning exprerience for me. If you observe me doing something here which is very obviously wrong or can suggest a more efficient and/or idiomatic approach, constructive criticism would be greatly appreciated.

**Fully implemented**:

- [F#](https://github.com/nathanchere/advent_of_code_polyglot_starter_kit/tree/master/templates/fsharp)
- [Julia](https://github.com/nathanchere/advent_of_code_polyglot_starter_kit/tree/master/templates/julia)
- [Powershell](https://github.com/nathanchere/advent_of_code_polyglot_starter_kit/tree/master/templates/powershell)
- [Python](https://github.com/nathanchere/advent_of_code_polyglot_starter_kit/tree/master/templates/python)

**Partly done**:

- C#
- Elixir
- Kotlin
- JavaScript

**TBC later**:

- D
- Crystal
- Go
- Haskell
- Rust
- Scala

**Maybe - interesting but low priority**:

- C/C++
- Erlang
- Idris
- Perl
- PHP
- R
- Ruby
- Swift

**Definitely not going to bother with**:

- Brainf\*ck
- LOLCODE
- Objective-C
- Visual Basic (any variant)

Certainly open to suggestions.

## Misc tips

If adding your own input files using Atom, configure or disable the `whitespace` package to stop newlines being added to the end of input files when saved.

Some good resources for comparing different languages:

- codecodex.com
- rosettacode.org
- hyperpolyglot.org

## TODO

- Handle network errors more gracefully in aoc-\* tools
- Better help with setting up Python and related e.g. pip and libs
- aoc wrapper
- ensure all languages work regardless of directory started from
- aoc-fetch to check if part 2 downloaded before re-checking
- show leaderboard after submitting a successful answer

## Credits

Thanks to:

- https://andrewlock.net/fixing-the-error-program-has-more-than-one-entry-point-defined-for-console-apps-containing-xunit-tests/ for salvaging my sanity while setting up the .Net Core test project

- https://github.com/tomswartz07/AdventOfCodeLeaderboard which saved me a lot of time writing the Slack integration for the aoc.py tool

- https://github.com/xunit/samples.xunit/blob/master/TestRunner/Program.cs which was largely stolen for the C# test runner
