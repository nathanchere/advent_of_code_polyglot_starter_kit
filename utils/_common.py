from os import path, makedirs
from shutil import copyfile
from tomd import Tomd
import config

PATH_BASE=path.realpath(path.dirname(__file__) + f'/..')
PATH_SOLUTIONS=f'{PATH_BASE}/solutions'
PATH_CHALLENGES=f'{PATH_BASE}/challenges'
PATH_UTILS=f'{PATH_BASE}/utils'
PATH_TEMPLATES=f'{PATH_BASE}/templates'

YEAR = config.YEAR
SESSION_ID = config.SESSION_ID
LEADERBOARD_ID = config.LEADERBOARD_ID
SLACK_WEBHOOK = config.SLACK_WEBHOOK
URL_BASE=f'https://adventofcode.com/{YEAR}'

def die(text):
    print(text)
    exit(1)

if not SESSION_ID:
    die('SESSION_ID must be set in config.py')

SESSION_COOKIE=dict(session=SESSION_ID)

def dayString(day):
    return str(day).rjust(2,'0')

def safeOpen(filePath):
    makedirs(path.dirname(filePath), exist_ok=True)
    return open(filePath, "w")

def safeCopyFile(path_from,path_to):
    if path.exists(path_to):
        print(f'{path_to} exist, skipping')
        return

    makedirs(path.dirname(path_to), exist_ok=True)
    copyfile(path_from, path_to)

def toMarkdown(html):
    return Tomd(html).markdown

def popArg(args, flag):
    if flag in args:
        args.remove(flag)
        return True
    return False