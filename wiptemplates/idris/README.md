# Advent of Code Starter Kit - LANG

## Quickstart

Assuming all statements are run from the repository root.

```
# Initialise a new LANG solution for Day 01 challenge
utils/aoc-init 1 LANG

# Run tests
#TODO

# Solve and print answer
# TODO
```

## Why use LANG?

ABOUT-LANG-URL

Some useful resources for learning LANG:

* LINKS

## Installing pre-requisites

Solutions assume LANG VERSION

Install LANG: (link to go here)