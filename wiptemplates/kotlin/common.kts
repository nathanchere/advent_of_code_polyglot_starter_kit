import java.io.File
import java.io.InputStream

fun getInput(inputfile: String): String {
  val file = File("../../challenges/$inputfile.input")
  return file.bufferedReader().use { it.readText() }
}