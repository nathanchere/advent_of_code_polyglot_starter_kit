// Advent of Code 2017 (Kotlin)
// {{{title}}} (Part {{{part}}})

@file:DependsOn("junit:junit:4.11")
@file:Include("common.kts")

import java.io.File
import java.io.InputStream
import org.junit.Test

fun solve(input: String): String {
  return input
}

/* /println(solve(getInput("template"))) */
/*if(args[0])*/

fun test(description: String, a: String, b: String) {
  if(a != b) {
    println("$description failed! Expected: '$a', actual: '$b'")
  }
  else {
    println("$description passed: $a")
  }
}

@Test
fun Example() {
  Assertions.assertEquals(1,2)
}

@Test
fun test(description: String, a: Int, b: Int) {
  if(a != b) {
    println("$description failed! Expected: $a, actual: $b")
  }
  else {
    println("$description passed: $a")
  }
}

if(args.size==1 && args[0]=="test"){
  test("Example", "a", "a")
  test("Example", "b", "a")
}else{
  println(solve(getInput("template")))
}

/* println(solve(getInput("day{{{day}}}"))) */