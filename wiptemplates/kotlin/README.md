# Advent of Code Starter Kit - Kotlin

## Quickstart

Assuming all statements are run from the repository root.

```
# Initialise a new Kotlin solution for Day 01 challenge
utils/aoc-init 1 kotlin

# Run tests
#TODO

# Solve and print answer
kscript day01a.kts
```

## Why use Kotlin?

https://kotlinlang.org

Some useful resources for learning Kotlin:

* https://kotlinlang.org/docs/tutorials/koans.html
* https://kotlinlang.org/docs/reference/idioms.html

## Installing pre-requisites

Solutions assume Kotlin 1.1

These solutions also use [kscript](https://github.com/holgerbrandl/kscript) to
simplify running solutions with minimal boilerplate.

Install Kotlin: (link to go here)

Install kscript: (link to go here)